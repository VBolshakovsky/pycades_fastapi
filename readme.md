# Проект для работы с SDKКриптоПро + fastapi

Для работы используеются библиотеки [pycades](https://docs.cryptopro.ru/cades/pycades) и [fastApi](https://fastapi.tiangolo.com/ru/), приложение docker-изировано

1. Для запуска необходимо выполнить
```
docker compose -f "docker-compose.yml" up -d --build 
```